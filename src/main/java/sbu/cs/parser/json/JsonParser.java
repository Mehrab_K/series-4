package sbu.cs.parser.json;

import java.util.*;

public class JsonParser
{
    private static List<JsonElementInterface> jsonElements = new ArrayList<>();

    public static Json parse(String data)
    {
        data = data.replaceAll("[\\s{}\"]*", "");

        String tmp = data;

        /**
         * this loop is because of arrays
         * it makes split simple
         */
        while(tmp.indexOf('[') >= 0)
        {
            String str = tmp.substring(tmp.indexOf("["), tmp.indexOf("]") + 1);

            data = data.replace(str, str.replaceAll(",", "#"));

            tmp = tmp.replace(str, "");
        }


        String [] datas = data.split(",");

        checkType(datas);

        Json json = new Json(jsonElements);

        jsonElements = new ArrayList<>();

        return json;
    }

    private static <V> void init(String key, V value)
    {
        JsonElement<V> jsonElement = new JsonElement<>(key, value);
        jsonElements.add(jsonElement);
    }

    private static void checkType(String[] datas)
    {
        for (String data : datas)
        {
            String key = data.split(":")[0];
            String value = data.split(":")[1];

            // check for arrays
            if(value.indexOf("]") >= 0)
            {
                jsonArray(key, value);
            }

            else if(value.matches("\\d*"))
            {
                init(key, Integer.parseInt(value));
            }

            else if(value.matches("\\d*\\.\\d*"))
            {
                init(key, Double.parseDouble(value));
            }

            else if(value.matches("(true|false)"))
            {
                init(key, Boolean.parseBoolean(value));
            }

            else
            {
                init(key, value);
            }
        }
    }


    private static void jsonArray(String key, String value)
    {
        String[] values = value.replace("[", "").replace("]", "").split("#");

        if (values[0].matches("\\d*"))
        {
            Integer[] newValues = new Integer[values.length];

            for(int i = 0; i < values.length; i++)
            {
                newValues[i] = Integer.parseInt(values[i]);
            }

            jsonElements.add(new JsonArray(key, newValues));
        }

        else if (values[0].matches("\\d*\\.\\d*"))
        {
            Double[] newValues = new Double[values.length];

            for(int i = 0; i < values.length; i++)
            {
                newValues[i] = Double.parseDouble(values[i]);
            }

            jsonElements.add(new JsonArray(key, newValues));
        }

        else if (values[0].matches("(true|false)"))
        {
            Boolean[] newValues = new Boolean[values.length];

            for(int i = 0; i < values.length; i++)
            {
                newValues[i] = Boolean.parseBoolean(values[i]);
            }

            jsonElements.add(new JsonArray(key, newValues));
        }

        else
        {
            jsonElements.add(new JsonArray(key, values));
        }
    }
}
