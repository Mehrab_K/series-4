package sbu.cs.parser.json;

public class JsonArray <V> implements JsonElementInterface
{
    private String key;
    private V[] values;


    public JsonArray(String key, V[] values)
    {
        this.key = key;
        this.values = values;
    }

    @Override
    public String getKey()
    {
        return this.key;
    }

    @Override
    public String getValue()
    {
        return toString();
    }


    @Override
    public String toString()
    {
        StringBuilder tmp = new StringBuilder("[");
        for(int i = 0; i < this.values.length; i++)
        {
            tmp.append(values[i]);
            if(i+1 != this.values.length)
                tmp.append(", ");
        }

        return tmp.append("]").toString();
    }
}
