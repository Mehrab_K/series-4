package sbu.cs.parser.json;

public interface JsonElementInterface
{
    String getKey();
    String getValue();
}
