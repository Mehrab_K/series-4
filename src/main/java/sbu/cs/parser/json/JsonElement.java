package sbu.cs.parser.json;


public class JsonElement <V> implements JsonElementInterface
{
    private String key;
    private V value;


    public JsonElement(String key, V value)
    {
        this.key = key;
        this.value = value;
    }

    @Override
    public String getKey()
    {
        return this.key;
    }

    @Override
    public String getValue()
    {
        return this.value.toString();
    }
}
