package sbu.cs.parser.json;

import java.util.*;

public class Json implements JsonInterface
{
    private List<JsonElementInterface> jsonElements;


    public Json(List<JsonElementInterface> jsonElements)
    {
        this.jsonElements = jsonElements;
    }

    @Override
    public String getStringValue(String key)
    {
        for (JsonElementInterface jsonElement : jsonElements)
        {
            if(jsonElement.getKey().equals(key))
            {
                return jsonElement.getValue();
            }
        }

        return null;
    }
}
