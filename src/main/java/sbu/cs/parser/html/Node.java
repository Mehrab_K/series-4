package sbu.cs.parser.html;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class Node implements NodeInterface
{
    private List<Node> children = new ArrayList<>();
    private String value;
    private Map<String, String> attributes = new HashMap<>();


    public Node(String value)
    {
        this.value = value;

        try
        {
            findAttr();
        }
        catch(Exception e)
        {

        }
    }

    public void addChild(Node child)
    {
        children.add(child);
    }


    @Override
    public String getStringInside()
    {
        if((value.indexOf('<') + 1) == (value.indexOf('/')))
            return null;

        String openTag = value.substring(value.indexOf('<'), value.indexOf('>') + 1);
        String tagName = value.substring(value.indexOf('<') + 1, Math.min(value.indexOf('>'), value.indexOf(' ')));
        String closeTag = "</" + tagName + ">";
        String tag = value.substring(value.indexOf(openTag) + openTag.length(), value.indexOf(closeTag)).trim();

        return tag;
    }


    @Override
    public List<Node> getChildren()
    {
        for(int i = 0; i < children.size(); i++)
        {
            Node tmp = HTMLParser.parse(children.get(i).value);
            children.set(i, tmp);
        }

        List<Node> copy = children;
        return copy;
    }


    @Override
    public String getAttributeValue(String key)
    {
        return attributes.get(key);
    }

    private void findAttr() throws Exception
    {
        if(!value.contains("="))
            return;

        String[] tmp = value.substring(value.indexOf(" ") + 1, value.indexOf(">") + 1).
                replace('>', ' ').split("\" ");

        for (String s : tmp)
        {
            s = s.replaceAll("\"", "");
            attributes.put(s.split("=")[0], s.split("=")[1]);
        }

    }
}
