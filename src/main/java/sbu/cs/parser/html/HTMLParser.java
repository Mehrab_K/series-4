package sbu.cs.parser.html;


public class HTMLParser
{
    public static Node parse(String document)
    {
        document = document.replaceAll("\n", "").trim();

        Node node = new Node(document);
        document = removeTagName(document);

        if(!document.contains("<"))
        {
            return node;
        }

        while(document.length() != 0)
        {
            node.addChild(openTag(document));
            document = deleteTag(document);
        }

        return node;
    }

    private static String removeTagName(String html)
    {
        String openTag = html.substring(html.indexOf('<'), html.indexOf('>') + 1);

        int tmp = Math.min(html.indexOf('>'), html.indexOf(' '));
        int index = tmp < 0 ? html.indexOf('>') : tmp;

        String tagName = html.substring(html.indexOf('<') + 1, index);
        String closeTag = "</" + tagName + ">";

        return html.replaceFirst(openTag, "").replaceFirst(closeTag, "");
    }


    private static Node openTag(String html)
    {
        String tmp = html.substring(html.indexOf('<'), html.indexOf('>') + 1);

        /**
         * this condition is for tags which do not get closed like :
         * </img . . . . . . . . . >
         */
        if((html.indexOf('<') + 1) == (html.indexOf('/')))
        {
            return new Node(tmp.trim());
        }

        else
        {
            String openTag = tmp;
            String tagName = html.substring(html.indexOf('<') + 1, Math.min(html.indexOf('>'), html.indexOf(' ')));
            String closeTag = "</" + tagName + ">";
            String tag = html.substring(html.indexOf(openTag) + openTag.length(), html.indexOf(closeTag)).trim();

            return new Node(openTag + tag + closeTag);
        }
    }


    private static String deleteTag(String html)
    {
        String temp = html.substring(html.indexOf('<'), html.indexOf('>') + 1);

        /**
         * this condition is for tags which do not get closed like :
         * </img . . . . . . . . . >
         */

        if((html.indexOf('<') + 1) == (html.indexOf('/')))
        {
            return html.replaceFirst(temp, "").trim();
        }

        else
        {
            String openTag = temp;
            String tagName = html.substring(html.indexOf('<') + 1, Math.min(html.indexOf('>'), html.indexOf(' ')));
            String closeTag = "</" + tagName + ">";
            String tag = html.substring(html.indexOf(openTag) + openTag.length(), html.indexOf(closeTag));

            return html.replaceFirst(openTag + tag + closeTag, "").trim();
        }
    }
}

